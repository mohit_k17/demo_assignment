import { Router } from '@angular/router';
import { Directive, HostBinding, HostListener } from '@angular/core';

@Directive({
  selector: '[appSubmenuToggle]'
})
export class SubmenuToggleDirective {

  @HostBinding('class.active') isOpen = false;
@HostListener('click') toggleOpen($event){
this.isOpen = !this.isOpen;
// console.log(this.router.url);
// if (this.router.url )
}

  constructor(private router: Router) { }

}
