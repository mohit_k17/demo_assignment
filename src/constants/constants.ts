export class ModalType {
  static get LARGE() { return 'modal-lg'; }
  static get MEDIUM() { return 'modal-md'; }
  static get SMALL() { return 'modal-sm'; }
}
