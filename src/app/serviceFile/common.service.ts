import { Injectable, EventEmitter } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { HttpErrorResponse, HttpClient } from '@angular/common/http';

import { CustomMessageService } from './custom-message.service';

import 'rxjs/add/operator/map';

@Injectable()
export class CommonService {
token;
headerToken;
headers;
options;
d = new Date();


constructor(public http: Http, private customMessage: CustomMessageService) { }

_showLoader: EventEmitter<boolean> = new EventEmitter<boolean>(true);
 
set showLoader(val: boolean) {
  this._showLoader.emit(val);
}
 /** show error message to the user. */
 public handleError(err: HttpErrorResponse) {
  // this.errorMessage = err.message;
  this.customMessage.showMessage({ severity: 'error', summary: 'error', detail: err.message });
}
}