import { Injectable, EventEmitter } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class ApiService {
token;
headerToken;
headers;
options;
d = new Date();


constructor(public http: Http) { }

 getToken(authRequired,utcOffset,contentTypeRequired){
   if(contentTypeRequired){
     this.headers = new Headers({ 'Content-Type': 'application/json', 'language': 'en' });
   }
   else{
    this.headers = new Headers({ 'Content-Type': undefined, 'language': 'en' });
    // this.headers = new Headers({ 'Content-Type': 'multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW' });
    
   }
        this.token = localStorage.getItem('userToken');
    this.headerToken = 'Bearer ' + this.token;
     if(authRequired === true && utcOffset === false)
     {
            this.headers = this.headers ? this.headers : {};
            this.headers.set('Authorization',this.headerToken);
            if(this.headers.has('utcoffset'))
            {
                this.headers.delete('utcoffset');
            }

     }
     else if (authRequired === true && utcOffset === true)
     {        
                this.headers.set('Authorization',this.headerToken);
                this.headers.set('utcoffset',-(this.d.getTimezoneOffset()));

     }
     else if (authRequired === false && utcOffset === false)
     {
         if(this.headers.has('Authorization'))
         {
             this.headers.delete('Authorization');
         }
         if(this.headers.has('utcoffset'))
            {
                this.headers.delete('utcoffset');
            }
     }
     else if (authRequired === false && utcOffset === true)
     {
         if(this.headers.has('Authorization'))
         {
             this.headers.delete('Authorization');
         }
         this.headers.set('utcoffset',(this.d.getTimezoneOffset()));
     }
   
      this.options = new RequestOptions({ headers: this.headers });
     return this.options;
 }

 


  getApi(url,authRequired,utcOffset,contentTypeRequired) {
    return this.http.get(url,this.getToken(authRequired,utcOffset,contentTypeRequired) )
      .map((res: Response) => {
        return res.json();
      })
  }

  getNewApi(url) {
    return this.http.get(url)
      .map((res: Response) => {
        return res.json();
      })
  }

  postApi(url,data,authRequired,utcOffset,contentTypeRequired) {
     return this.http.post(url,data,this.getToken(authRequired,utcOffset,contentTypeRequired))
      .map((res: Response) => {
        // console.log(res.headers);
        if(res.headers) {
          const accessToken = res.headers.get('authorization');
          localStorage.setItem('userToken', accessToken);
        }
        return res.json();
      })
  }
  deleteApi(url,data,authRequired,utcOffset,contentTypeRequired) {
      this.getToken(authRequired,utcOffset,contentTypeRequired);
      this.options = new RequestOptions({ 'headers': this.headers,
      'body': { 'userID': data.userID } });
     return this.http.delete(url,this.options)
        .map((res: Response) => res.json())
  }

  putApi(url,data,authRequired,utcOffset,contentTypeRequired) {
      return this.http.put(url, data, this.getToken(authRequired,utcOffset,contentTypeRequired))
      .map((res: Response) => res.json())
  }

  patchApi(url,data,authRequired,utcOffset,contentTypeRequired) {
    return this.http.patch(url, data, this.getToken(authRequired,utcOffset,contentTypeRequired))
    .map((res: Response) => res.json())
}
}
