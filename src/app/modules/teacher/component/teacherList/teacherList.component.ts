import { Component, OnInit, ViewChild, ɵConsole, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormsModule } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { LazyLoadEvent } from 'primeng/primeng';
import { Table } from 'primeng/table';
import { SelectItem } from 'primeng/api';

import { TeacherService } from '../../serviceFile/teacher.service';
import { AngularFontAwesomeComponent } from 'angular-font-awesome';
import { CustomMessageService } from '../../../../serviceFile/custom-message.service';
import { CommonService } from '../../../../serviceFile/common.service';
declare var $:any;

@Component({
  selector: 'app-teacherList',
  templateUrl: './teacherList.component.html',
  styleUrls: ['./teacherList.component.scss']
})
export class TeacherListComponent implements OnInit {
  @ViewChild('ptable') ptable: Table;


  alreadySearch = false;
  isFilterFlag = false;
  isSearchFlag = false;
  selectedTeacherData;
  popupType;
  maxDateTillToday;
  cols: { field: string; header: string; terminology?: Array<string> }[];
  
  noCheckBoxSelected: boolean = false;

  schoolId:any;
  schoolKey:any;

  namesData:any;
  

  constructor( private formBuilder: FormBuilder, private teacherService: TeacherService, private customMessage: CustomMessageService,
     private router: Router, private commonService: CommonService) {
      this.schoolId = localStorage.getItem('schoolId');
      this.schoolKey = localStorage.getItem('schoolKey');
      

    let todayDate = new Date();
    this.maxDateTillToday = todayDate;
  }
 
  imagesBasic;

  ngOnInit() {
    this.getDemoData();

    this.imagesBasic = [
      { img: "https://mdbootstrap.com/img/Photos/Horizontal/Nature/12-col/img%20(117).jpg", thumb: "https://mdbootstrap.com/img/Photos/Horizontal/Nature/12-col/img%20(117).jpg", description: "Image 1" },
     
    ];

    // this.getAllTitle();
  }

  private _albums = [];
  // show Image model

  showModal: boolean;
  showImage : ""
  show(event)
  {debugger
    this.showModal = true; // Show-Hide Modal Check
    this.showImage = event.target.src
  }



  searchTeacherName;

  keyDownFunction(event) {
    if(event.keyCode == 13) {
      if(this.searchTeacherName){
        this.alreadySearch = true;
        this.getDemoData();
      }else if(this.alreadySearch == true){
        this.alreadySearch = false;
        this.getDemoData();
      }else return;
    }
  }
  

  
/**
   * on primeng table lazy load event
   * @param event primeng LazyLoadEvent
   */
  loadServiceLazy(event: LazyLoadEvent) {
   
    this.getDemoData();
  }

  infotext;
/**
   * shows text as 'showing 1 of 2 entries'
   * @param totalRecords total entities available
   */
  protected setInfoText(totalRecords) {
    const _of = (this.ptable.rows + this.ptable.first) > totalRecords ? totalRecords : (this.ptable.rows + this.ptable.first);
    this.infotext = 'Showing '+(this.ptable.first + 1) +'-'+ _of+ ' of '+ totalRecords+' entries';
  }

  

  emptySearchData(val){
    if(!val && this.isSearchFlag){
      this.getDemoData();
    }
}


  getDemoData(){
    this.commonService.showLoader = true;

    console.log(this.searchTeacherName , "searchTeacherName")


    this.teacherService.getAllNames(this.searchTeacherName)
      .subscribe(response => {
        this.commonService.showLoader = false;
        this.namesData = response.drinks;
        
      }, error => {
        // const errorMsg = JSON.parse(error._body);
        
      });
  }

}
