import { NgModule } from '@angular/core';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { LazyLoadEvent, CalendarModule } from 'primeng/primeng';
import { TableModule } from 'primeng/table';
import { DropdownModule } from 'primeng/dropdown';
import {MultiSelectModule} from 'primeng/multiselect';
import {OverlayPanelModule} from 'primeng/overlaypanel';

import { LightboxModule } from 'ngx-lightbox';

// import { MDBBootstrapModulesPro } from 'screenfull';


import { TeacherListComponent } from './component/teacherList/teacherList.component';
import { TeacherService } from './serviceFile/teacher.service';

export const routes: Routes = [
  {
    path: 'list',
    component: TeacherListComponent
  },
  {
    path: '',
    redirectTo: 'list',
    pathMatch: 'full'
  }
];
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes),
    TableModule,
    DropdownModule,
    OverlayPanelModule,
    MultiSelectModule,
    CalendarModule,
    LightboxModule
  ],
  declarations: [
    TeacherListComponent
    ],
  providers: [ TeacherService ],
  exports: [
    TeacherListComponent

  ]
})
export class TeacherModule { 
    
}
