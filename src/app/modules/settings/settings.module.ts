import { OverlayPanelModule } from 'primeng/overlaypanel';


import { DropdownModule } from 'primeng/dropdown';
import { SettingsService } from './serviceFile/settings.service';
import { TableModule } from 'primeng/table';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import {NgxImageCompressService} from 'ngx-image-compress';
import { UniversityComponent } from './component/university/university.component';

const routes: Routes = [
    { path: 'university', component: UniversityComponent },

];

@NgModule({
    imports: [CommonModule,
        RouterModule.forChild(routes),
        FormsModule,
        ReactiveFormsModule,
        TableModule,
        DropdownModule
    ],
    declarations: [
        UniversityComponent
    
       ],
    providers: [SettingsService,NgxImageCompressService],
    exports: [
        
        UniversityComponent,
        
    ]
})

export class SettingsModule {}
