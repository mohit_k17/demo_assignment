import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormsModule } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { LazyLoadEvent } from 'primeng/primeng';
import { Table } from 'primeng/table';
import { SelectItem } from 'primeng/api';

import { SettingsService } from './../../serviceFile/settings.service';
import { AngularFontAwesomeComponent } from 'angular-font-awesome';
import { CustomMessageService } from '../../../../serviceFile/custom-message.service';
import { CommonService } from '../../../../serviceFile/common.service';
// import { OpenConfirmationDialogComponent } from '../openConfirmationDialog/openConfirmationDialog.component';


@Component({
  selector: 'app-university',
  templateUrl: './university.component.html',
  styleUrls: ['./university.component.scss']
})
export class UniversityComponent implements OnInit {


  @ViewChild('ptable') ptable: Table;
  
  drinksData:any;
  maxDateTillToday;
  

  constructor( private formBuilder: FormBuilder, private schoolService: SettingsService, private customMessage: CustomMessageService,
     private router: Router, private commonService: CommonService) {
    
    let todayDate = new Date();
    this.maxDateTillToday = todayDate;


  }
 
  ngOnInit() {
    this.getDemoData();
  }



 

  
/**
   * on primeng table lazy load event
   * @param event primeng LazyLoadEvent
   */
  loadServiceLazy(event: LazyLoadEvent) {
    this.getDemoData();
  }

  infotext;
/**
   * shows text as 'showing 1 of 2 entries'
   * @param totalRecords total entities available
   */
  protected setInfoText(totalRecords) {
    const _of = (this.ptable.rows + this.ptable.first) > totalRecords ? totalRecords : (this.ptable.rows + this.ptable.first);
    this.infotext = 'Showing '+(this.ptable.first + 1) +'-'+ _of+ ' of '+ totalRecords+' entries';
  }



  getDemoData(){
    this.commonService.showLoader = true;

    this.schoolService.getAllNames()
      .subscribe(response => {
        this.commonService.showLoader = false;
        this.drinksData = response.drinks;
        
      }, error => {
        // const errorMsg = JSON.parse(error._body);
        
      });
  }

  
}
