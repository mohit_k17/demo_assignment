import { Injectable, Output, EventEmitter } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Http, Headers, RequestOptions, Response } from '@angular/http';

import { ApiService } from '../../../serviceFile/api.service';
import 'rxjs/add/operator/map';


@Injectable()
export class SettingsService {
  authRequired;
  utcOffset;
  contentTypeRequired;
  token;
  headerToken;
  headers;
  options;
  @Output() _hidePopup: EventEmitter<any> = new EventEmitter<any>();

  constructor(private http: HttpClient, private apiService: ApiService) { }


  getAllNames(){
    return this.apiService.getNewApi("https://www.thecocktaildb.com/api/json/v1/1/filter.php?c=Cocktail");
  }


}
