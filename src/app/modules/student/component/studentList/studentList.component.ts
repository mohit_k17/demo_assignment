import { Component, OnInit, ViewChild, ɵConsole, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormsModule } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { LazyLoadEvent } from 'primeng/primeng';
import { Table } from 'primeng/table';
import { SelectItem } from 'primeng/api';

import { StudentService } from '../../serviceFile/student.service';
import { AngularFontAwesomeComponent } from 'angular-font-awesome';
import { CustomMessageService } from '../../../../serviceFile/custom-message.service';
import { CommonService } from '../../../../serviceFile/common.service';
declare var $:any;

@Component({
  selector: 'app-studentList',
  templateUrl: './studentList.component.html',
  styleUrls: ['./studentList.component.scss']
})
export class StudentListComponent implements OnInit {
  @ViewChild('ptable') ptable: Table;
  maxDateTillToday;

  constructor( private formBuilder: FormBuilder, private studentService: StudentService, private customMessage: CustomMessageService,
     private router: Router, private commonService: CommonService) {



    let todayDate = new Date();
    this.maxDateTillToday = todayDate;
  }
 
  
  ngOnInit() {
    this.getDemoData();
  }
  
  ingData:any;



 
/**
   * on primeng table lazy load event
   * @param event primeng LazyLoadEvent
   */
  loadServiceLazy(event: LazyLoadEvent) {
    this.getDemoData();
  }

  infotext;
/**
   * shows text as 'showing 1 of 2 entries'
   * @param totalRecords total entities available
   */
  protected setInfoText(totalRecords) {
    const _of = (this.ptable.rows + this.ptable.first) > totalRecords ? totalRecords : (this.ptable.rows + this.ptable.first);
    this.infotext = 'Showing '+(this.ptable.first + 1) +'-'+ _of+ ' of '+ totalRecords+' entries';
  }



  getDemoData(){
    this.commonService.showLoader = true;

    this.studentService.getAllNames()
      .subscribe(response => {
        this.commonService.showLoader = false;
        this.ingData = response.drinks;
        
      }, error => {
        // const errorMsg = JSON.parse(error._body);
        
      });
  }
 
 
 
  }


