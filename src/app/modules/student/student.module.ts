import { NgModule } from '@angular/core';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { LazyLoadEvent, CalendarModule } from 'primeng/primeng';
import { TableModule } from 'primeng/table';
import { DropdownModule } from 'primeng/dropdown';
import {MultiSelectModule} from 'primeng/multiselect';
import {OverlayPanelModule} from 'primeng/overlaypanel';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';

import { StudentListComponent } from './component/studentList/studentList.component';
import { StudentService } from './serviceFile/student.service';


export const routes: Routes = [
  {
    path: 'list',
    component: StudentListComponent
  },
  {
    path: '',
    redirectTo: 'list',
    pathMatch: 'full'
  }
];
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes),
    TableModule,
    DropdownModule,
    OverlayPanelModule,
    MultiSelectModule,
    CalendarModule,
    NgMultiSelectDropDownModule
  ],
  declarations: [
    StudentListComponent
    ],
  providers: [ StudentService ],
  exports: [
    StudentListComponent

  ]
})
export class StudentModule { 
    
}
