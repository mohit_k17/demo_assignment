import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
// import { environment } from '../../../environments/environment';
import { ApiService } from '../../serviceFile/api.service';


@Injectable()
export class LoginService {
  authRequired;
  utcOffset;
  contentTypeRequired;
  constructor(private http: HttpClient, private apiService: ApiService) { }

  /**
   * function to login
   * @param object object containing login data
   */
  login(payload) {
    console.log(payload , "payload")
    // let url = environment.api_url+'/Admins/Login';
    this.authRequired = false;
    this.contentTypeRequired = true;
    this.utcOffset = false;
    return this.apiService.postApi('http://localhost:4200/',payload, this.authRequired, this.utcOffset, this.contentTypeRequired);
  }

}
