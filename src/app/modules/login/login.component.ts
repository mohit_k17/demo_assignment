import { Component, OnInit } from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormsModule } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { LoginService } from './login.service';
import { CustomMessageService } from '../../serviceFile/custom-message.service';
import { CommonService } from '../../serviceFile/common.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  
  public loginForm: FormGroup;
  public notSame: boolean;
  public userId: any;
  submitted = false;
 
  constructor( private formBuilder: FormBuilder, private commonService: CommonService, private loginService: LoginService, private customMessage: CustomMessageService, private route: Router ) {
     
  }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.pattern('^([_a-zA-Z0-9-+]+)(\.[_a-zA-Z0-9-+]+)*@([a-zA-Z0-9][a-zA-Z0-9-]*\.)+((?!([Ww][eE][bB]))[a-zA-Z]{2,3})$')]],
      password: ['', [Validators.required,  Validators.pattern('^(?=.*?[A-Z]).{6,}$') ]]
    });
  }

  get f() { return this.loginForm.controls; }
  
  submitLoginForm() { 
    this.submitted = true;
    
    if (!this.loginForm.valid) {
      return;
    }
    let payload = {
      password : 'M123456',
      email : 'mohit@test.com'
    };
    this.commonService.showLoader = true;
    
    this.loginService.login(payload)
      .subscribe(response => {
        console.log(response , "response");
        this.route.navigateByUrl('/page/settings/university');
  
      }, error => {
        // console.log(JSON.parse(error._body));
        this.commonService.showLoader = false;
        const errorMsg = JSON.parse(error._body);
        this.customMessage.showMessage({ severity: 'error', detail: errorMsg.message});
      });
  }

}
