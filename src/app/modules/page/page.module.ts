import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

import { PageRoutingModule } from './page-routing.module';

import { SidebarModule } from '../sidebar/sidebar.module';
import { PageComponent } from './page.component';

@NgModule({
  imports: [
    CommonModule,
    PageRoutingModule,
    SidebarModule
  ],
  declarations: [
    PageComponent
  ],
  providers: []
})
export class PageModule { }
