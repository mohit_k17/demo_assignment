import { Component, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-page',
  templateUrl: './page.component.html',
  styleUrls: [
  '../../../../node_modules/primeng/resources/components/common/common.css',
  '../../../../node_modules/primeng/resources/components/table/table.css',
  '../../../../node_modules/primeng/resources/components/paginator/paginator.css',
  '../../../../node_modules/primeng/resources/components/calendar/calendar.css',
  '../../../../node_modules/primeng/resources/components/dropdown/dropdown.css',
  '../../../../node_modules/primeng/resources/components/multiselect/multiselect.css',
  '../../../../node_modules/primeng/resources/components/tabview/tabview.css',
  '../../../../node_modules/font-awesome/scss/font-awesome.scss',
  '../../../../node_modules/primeicons/primeicons.css',
    '../../theme/sass/table.scss',
],
  encapsulation: ViewEncapsulation.None
})
export class PageComponent {

}
