import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PageComponent } from './page.component';

const routes: Routes = [
    {
        path: '',
        component: PageComponent,
        children: [
            // { path: 'dashboard', loadChildren: '../dashboard/dashboard.module#DashboardModule' },
            { path: 'settings', loadChildren: '../settings/settings.module#SettingsModule' },
            { path: 'student', loadChildren: '../student/student.module#StudentModule' },
            { path: 'teacher', loadChildren: '../teacher/teacher.module#TeacherModule' },
            // { path: 'newpost', loadChildren: '../newPost/newPost.module#NewPostModule' },
            
            // { path: 'transaction', loadChildren: '../transaction/transaction.module#TransactionModule'},
            { path: '', redirectTo: 'dashboard', pathMatch: 'full' }
        ]
    }
];
@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class PageRoutingModule { }