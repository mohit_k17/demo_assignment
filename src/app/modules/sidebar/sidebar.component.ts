import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {
  caretUp: boolean = false;
  libraryCaretUp: boolean = false;

  constructor() {

  }

  ngOnInit() {

  }

  studentSelect(){
    localStorage.setItem('schoolKey' , 'true');
  }

  alignCaret(val) {
    this.caretUp = !val;
    this.libraryCaretUp = !val;
  }


}

