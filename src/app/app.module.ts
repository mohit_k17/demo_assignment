import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { GrowlModule } from 'primeng/growl';


import { LoginGuard } from './guards/login.guard';
import { AuthGuard } from './guards/authguardservice.guard';
import { ApiService } from './serviceFile/api.service';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { CommonService } from './serviceFile/common.service';
import { CustomMessageService } from './serviceFile/custom-message.service';
import { MessageService } from 'primeng/components/common/messageservice';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';



@NgModule({
  declarations: [
    AppComponent,
 
  ],
  imports: [
    // AgmCoreModule.forRoot({
    //   apiKey: "AIzaSyC1FKw07uyUwVgDQY2L5ATeoEcYk2Sw6KI",            //AIzaSyAslPdlXbA7L9qr-LpU3n4ukYIUSgD5SZs
    //   libraries: ["places"]
    // }),
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    HttpModule,
    HttpClientModule,
    AngularFontAwesomeModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
    GrowlModule
   
  ],
  providers: [ AuthGuard, LoginGuard, ApiService, CommonService, CustomMessageService, MessageService ],
  bootstrap: [AppComponent]
})
export class AppModule { }
