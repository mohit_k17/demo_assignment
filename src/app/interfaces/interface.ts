export interface IModalType {
  LARGE: string;
  MEDIUM: string;
  SMALL: string;
}

