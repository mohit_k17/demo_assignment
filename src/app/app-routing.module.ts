import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AuthGuard } from './guards/authguardservice.guard';
import { LoginGuard } from './guards/login.guard';

const routes: Routes = [
  {
    path: 'login', loadChildren: './modules/login/login.module#LoginModule', canLoad: [LoginGuard]
  },

  {
    path: 'page', loadChildren: './modules/page/page.module#PageModule'
  },
  { path: '', redirectTo: 'page/settings/university', pathMatch: 'full' },

];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}

