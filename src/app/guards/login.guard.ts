import { Injectable } from '@angular/core';
import { CanLoad, CanActivate, Router, Route, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

@Injectable()
export class LoginGuard implements CanLoad {

  constructor(private router: Router) {

  }
  canLoad(route: Route) {
    // console.log(route);
    let storage: any = localStorage.getItem('userToken');
    if (storage) {
      this.router.navigate(['/page']);
      return false;
    }
    return true;
  }

}
